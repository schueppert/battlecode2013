package team291;
import battlecode.common.*;

public class RobotPlayer{
  
  /////////////////////////////////////////////////////////////////////////////////
  //VARIABLE DECLARATIONS
  /////////////////////////////////////////////////////////////////////////////////
  static RobotController rc;
  static Direction spawnDirection;
  static MapLocation myHQLocation;
  static MapLocation enemyHQLocation;
  static MapLocation target;
  static Team myTeam;
  static Team enemyTeam;
  static int stuck = 0;
 
  /////////////////////////////////////////////////////////////////////////////////
  //MAIN RUN LOOP
  /////////////////////////////////////////////////////////////////////////////////
	public static void run(RobotController myRC){
	  rc = myRC;
		myHQLocation = rc.senseHQLocation();
    enemyHQLocation = rc.senseEnemyHQLocation();
    target = enemyHQLocation;
    myTeam = rc.getTeam();
    enemyTeam = myTeam.opponent();
	
		while(true){
			try{
			  if(rc.getType()==RobotType.SOLDIER){
			      if(Clock.getRoundNum()<5){supplierCode();}
          	else {soldierCode();}
        	}
			  if(rc.getType()==RobotType.HQ){hqCode();}
			  rc.yield();
			}
			catch (Exception e){
				System.out.println("caught exception before it killed us:");
				e.printStackTrace();
			}
			rc.yield();
		}
	}
	
	/////////////////////////////////////////////////////////////////////////////////
  //SUPPLIER CODE
  /////////////////////////////////////////////////////////////////////////////////
	static void supplierCode() throws GameActionException{
		  
	  MapLocation[] camps = rc.senseEncampmentSquares(myHQLocation,200,null);
	  if (camps.length>0){
	    int closestDist = 200;
    	MapLocation closestCamp = null;
  	  for (MapLocation camp:camps){
    		int dist = camp.distanceSquaredTo(myHQLocation);
    		if ((dist<closestDist)&&(dist>2)){
    			closestDist = dist;
    			closestCamp = camp;
    		}
    	}
    	target = closestCamp;
	  }
	  else {target = enemyHQLocation;}
		  
		while(true){
			try{
			  if(rc.isActive()){
			    if(rc.getLocation().equals(target)){rc.captureEncampment(RobotType.SUPPLIER);}
			    else{
			      move();
			      }
			    }
			  rc.yield();
			}
			catch (Exception e){
				System.out.println("Soldier Exception");
				e.printStackTrace();
			}
			rc.yield();
		}
	}
	
	/////////////////////////////////////////////////////////////////////////////////
  //SOLDIER CODE
  /////////////////////////////////////////////////////////////////////////////////
	static void soldierCode() throws GameActionException{
		  
		while(true){
			if(rc.isActive()){
			  try{move();}
			  catch (Exception e){
				System.out.println("Soldier Exception");
				e.printStackTrace();
			  }
			}
			rc.yield();
		}
	}
	
	/////////////////////////////////////////////////////////////////////////////////
  //HQ CODE
  /////////////////////////////////////////////////////////////////////////////////
	static void hqCode() throws GameActionException{
		spawnDirection = myHQLocation.directionTo(enemyHQLocation);
    if(mineAhead(spawnDirection)){
      spawnDirection = spawnDirection.rotateRight();
      }	
		while(true){
			try{
			  if (rc.isActive()) {
			    boolean shouldSpawn =rc.getTeamPower()>40||Clock.getRoundNum()<10;
			    if(shouldSpawn&&rc.canMove(spawnDirection)){
			      rc.spawn(spawnDirection);
			      }
			    else{rc.researchUpgrade(Upgrade.NUKE);}
			  }
			}
			catch (Exception e){
				System.out.println("HQ Exception");
				e.printStackTrace();
			}
			rc.yield();
		}
	}

  /////////////////////////////////////////////////////////////////////////////////
  //UTILITIES
  /////////////////////////////////////////////////////////////////////////////////
  static MapLocation findClosestEnemy(Bot[] enemy) throws GameActionException{
    int closestDist = 10000;
  	MapLocation closestEnemy=null;
	  for (Bot bot:enemy){
  		int dist = rc.getLocation().distanceSquaredTo(bot.location);
  		if (dist<closestDist){
  			closestDist = dist;
  			closestEnemy = bot.location;
  		}
  	}
  	return closestEnemy;
  }
  
  static boolean mineAhead(Direction dir){
    MapLocation ahead = rc.getLocation().add(dir);
  	Team mineAhead = rc.senseMine(ahead);
  	if(mineAhead!=null&&mineAhead!=myTeam){return true;}
  	else{return false;}
  }
  
  static int adjacentBots(MapLocation loc,Bot[]bots)throws GameActionException{
    int count = 0;
    for(Bot bot:bots){
      if(loc.isAdjacentTo(bot.location)){
        count++;
      }
    }
    return count;
  }
  
  static int badBots(MapLocation loc,Bot[]bots)throws GameActionException{
    int count = 0;
    for(Bot bot:bots){
      int dist = loc.distanceSquaredTo(bot.location);
      if(dist>2&&dist<9&&!bot.isEngaged&&bot.isSoldier){
        count++;
      }
    }
    return count;
  }
  
  static Bot[] makeBotArray(Robot[] robots)throws GameActionException{
    Bot[] bots = new Bot[robots.length];
    for(int i=robots.length-1;i>=0;i--){
      RobotInfo info = rc.senseRobotInfo(robots[i]);
      bots[i] = new Bot(info);
    }
    return bots;
  }
  
  static Bot[]markEngaged(Bot[]myBots,Bot[]enemyBots)throws GameActionException{
      for(Bot myBot:myBots){
        for(Bot enemyBot:enemyBots){
          if(myBot.location.isAdjacentTo(enemyBot.location)){
            enemyBot.isEngaged = true;
          }
        }
      }
    return enemyBots;
  }
  
  /////////////////////////////////////////////////////////////////////////////////
  //MOVE
  /////////////////////////////////////////////////////////////////////////////////
  static void move() throws GameActionException{
    MapLocation myLocation = rc.getLocation();
    Robot[] enemyRobots = rc.senseNearbyGameObjects(Robot.class,myLocation,50,enemyTeam);
    
    if(enemyRobots.length==0){
      Direction fwd = myLocation.directionTo(target);
      Direction right = fwd.rotateRight();
      Direction left = fwd.rotateLeft();
      Direction best = fwd;
      int mineCount = rc.senseNonAlliedMineLocations(myLocation.add(fwd).add(fwd),10).length;
      int newCount = rc.senseNonAlliedMineLocations(myLocation.add(right).add(right),10).length;
      if(newCount<mineCount){
        mineCount = newCount;
        best = right;
      }
      newCount = rc.senseNonAlliedMineLocations(myLocation.add(left).add(left),10).length;
    
      if(newCount<mineCount){
        best = left;
      }    
      if(myLocation.add(fwd).equals(target)){best = fwd;}
      Direction[] options = {best,fwd,left,right};
      for(Direction option:options){
        if(rc.canMove(option)&&!mineAhead(option)){
          rc.move(option);
          stuck = 0;
          return;
        }
      }
      for(Direction option:options){
        if(rc.canMove(option)){
          rc.defuseMine(myLocation.add(option));
          stuck = 0;
          return;
        }
      }   
    }
    
    else{
      Robot[] myRobots = rc.senseNearbyGameObjects(Robot.class,myLocation,51,myTeam);
      Bot[] enemyBots = makeBotArray(enemyRobots);
      Bot[] myBots = makeBotArray(myRobots);
      enemyBots = markEngaged(myBots,enemyBots);
      MapLocation closestEnemy = findClosestEnemy(enemyBots);
      Direction[] options = Direction.values();
      boolean standingOnMine = (rc.senseMine(myLocation)!=null);
      
      int score = 0;
      if(adjacentBots(myLocation,myBots)>2){score+=1;};
      score -= badBots(myLocation,enemyBots);
      int enemyAdjacent = adjacentBots(myLocation,enemyBots);
      if(enemyAdjacent==1){score+=2;}
      else score -= enemyAdjacent;
      
      int dist = myLocation.distanceSquaredTo(closestEnemy);

      Direction best = Direction.NONE;
      if(standingOnMine){best = Direction.NORTH;}//anything but none
     
        
      for(Direction option:options){
        //Avoid impossible moves
        if(option==Direction.NONE||option==Direction.OMNI){continue;}
        //Avoid robots and mines
        if(!rc.canMove(option)||mineAhead(option)){continue;}
        MapLocation newLocation = myLocation.add(option);
        int newScore = 0;
        if(adjacentBots(newLocation,myBots)>2){score+=1;};
        newScore -= badBots(newLocation,enemyBots);
        int newEnemyAdjacent = adjacentBots(newLocation,enemyBots);
        if(rc.getEnergon()<7&&newEnemyAdjacent>0){newScore+=10;}//Kamikazee
        if(newEnemyAdjacent==1){newScore+=2;}
        else newScore -= newEnemyAdjacent;
        if (newScore>score){best=option;score=newScore;}
        int newDist = newLocation.distanceSquaredTo(closestEnemy);
        if(newScore==score&&newDist<dist){best=option;score=newScore;dist=newDist;}
      }
      
      if(best!=Direction.NONE&&best!=Direction.OMNI){
        if(rc.canMove(best)){
          rc.move(best);
          stuck = 0;
        }
      }
      else{
        stuck++;
        if(stuck>3){
          Direction dir = myLocation.directionTo(closestEnemy);
          if(rc.canMove(dir)){
            if(mineAhead(dir)){rc.defuseMine(myLocation.add(dir));}
            else{rc.move(dir);}
            stuck = 0;
          }
        }
      }
      
    }//end of big else (move vs.combat)
  }//end of Run Method
}//end of Class





