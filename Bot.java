package team291;
import battlecode.common.*;

/////////////////////////////////////////////////////////////////////////////////
 //Bot Class
 /////////////////////////////////////////////////////////////////////////////////
public class Bot {
  public MapLocation location;
  public boolean isEngaged = false;
  public boolean isSoldier = false;
  
  public Bot(RobotInfo info){
    location = info.location;
    if(info.type==RobotType.SOLDIER){isSoldier=true;}
  }
}